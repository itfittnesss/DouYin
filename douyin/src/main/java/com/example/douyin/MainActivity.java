package com.example.douyin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    NavItemView navItemView1,navItemView2,navItemView3,navItemView4,navItemView_Temp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navItemView1=findViewById(R.id.one);
        navItemView2=findViewById(R.id.two);
        navItemView3=findViewById(R.id.three);
        navItemView4=findViewById(R.id.four);
        navItemView1.setShowReflashImage(true);
        navItemView1.setNavItemReflashListener(new NavItemReflashListener() {
            @Override
            public void onReflash(View v) {
                Toast.makeText(MainActivity.this, "刷新", Toast.LENGTH_SHORT).show();
            }
        });
        navItemView1.setOnClickListener(this);
        navItemView2.setOnClickListener(this);
        navItemView3.setOnClickListener(this);
        navItemView4.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if(navItemView_Temp!=null){
            navItemView_Temp.cancelActive();
        }
        switch (v.getId()){
            case R.id.one:
                navItemView1.startActive();
                navItemView_Temp=navItemView1;
                break;
            case R.id.two:
                navItemView2.startActive();
                navItemView_Temp=navItemView2;
                break;
            case R.id.three:
                navItemView3.startActive();
                navItemView_Temp=navItemView3;
                break;
            case R.id.four:
                navItemView4.startActive();
                navItemView_Temp=navItemView4;
                break;
        }
    }
}
